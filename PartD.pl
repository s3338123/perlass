#!/usr/bin/env perl

use warnings;
use strict;
use Fcntl ":flock";

=pod

=head1 NAME

PartD.pl - Script that parses other perl scripts, highlighting vital information for the CPT223 community

=head1 B<USAGE>

Suppose the script is executable in the current path:

C<PartD.pl sample.pl>

=head1 B<OUTPUT>

Running this script will output the script passed into the command line (first argument) and output it to the HTML file passed into the command line
(second argument). The HTML output will highlight the following information

=over 4

=item *

Any perl keywords (as defined by assignment specs) that appear in the provided script will be highglighted in dark red (color=darkred).

=item *

Any numerals (as defined by assignment specs) that appear in the provided script will be highlighted in dark cyan (color=darkcyan)

=item *

Any strings (as defined by assignment specs) that appear in the provided script will be highlighted in royal blue (color=royalblue)

=item *

Any comments (you guessed it.. as defined by assignment specs) that appear in the provided script will be highlighted in dark green (color=darkgreen)

=back

=head3 B<--AUTHOR>

Luke Herron
S3338123

=cut

 # Handle incorrect script usage
die "\nUsage : PartD.pl perl_script html_output\n" if $#ARGV + 1 != 2;
my $script = $ARGV[0];
my $output = $ARGV[1];

open my $fh, $script or die "File Operation Failed: $!";
flock($fh, LOCK_SH);

die "Error: unable to analyse the specified file.\n" if not -s $fh && not -r $fh && $script !~ /\.pl|\.pm$/i || $output !~ /\.htm|\.html$/i;

# Engage slurp and get the file contents into one big string
local $/;
my $contents = <$fh>;
close $fh;

my $lineCount = () = $contents =~ m/\n/g;	# Match all non whitespace chars (grouped into 'words'), assign the results to a list, then return scalar value of list (which will store count of list elements)
my $wordCount = () = $contents =~ m/\S+/g;	# As above, except this time we are matching all non whitespace chars (grouped into 'words')
my $charCount = () = $contents =~ m/./gms;	# As above, except this time we are matching every single character in the line, including whitespace.

# Import the hardcoded keyword array into a hash
my %keywords;
$keywords{$_}++ for (
	"__DATA__", "__END__", "__FILE__", "__LINE__", "__PACKAGE__", "A", "abs", "accept", "alarm", "and", "ARGV", "ARGVOUT", "atan2", "AUTOLOAD", "B", "b", "BEGIN", "bind", "binmode", "bless", "break", "C", "c", "caller", 
	"chdir", "CHECK", "chmod", "chomp", "chop", "chown", "chr", "chroot", "close", "closedir", "cmp", "connect", "continue", "CORE", "cos", "crypt", "d", "dbmclose", "dbmopen", "defined", "delete", "DESTROY", "die", "do",
	"dump", "e", "each", "else", "elsif", "END", "endgrent", "endhostent", "endnetent", "endprotoent", "endpwent", "endservent", "eof", "eq", "eval", "exec", "exists", "exit", "exp", "f", "fcntl", "fileno", "flock", "for",
	"foreach", "fork", "format", "formline", "g", "ge", "getc", "getgrent", "getgrgid", "getgrnam", "gethostbyaddr", "gethostbyname", "gethostent", "getlogin", "getnetbyaddr", "getnetbyname", "getnetent", "getpeername",
	"getpgrp", "getppid", "getpriority", "getprotobyname", "getprotobynumber", "getprotoent", "getpwent", "getpwnam", "getpwuid", "getservbyname", "getservbyport", "getservent", "getsockname", "getsockopt", "glob",
	"gmtime", "goto", "grep", "gt", "hex", "if", "index", "INIT", "int", "ioctl", "join", "k", "keys", "kill", "l", "last", "lc", "lcfirst", "le", "length", "link", "listen", "local", "localtime", "lock", "log", "lstat",
	"lt", "M", "m", "map", "mkdir", "msgctl", "msgget", "msgrcv", "msgsnd", "my", "ne", "next", "no", "not", "O", "o", "oct", "open", "opendir", "or", "ord", "our", "p", "pack", "package", "pipe", "pop", "pos", "print",
	"printf", "prototype", "push", "q", "qq", "qr", "quotemeta", "qw", "qx", "r", "R", "rand", "read", "readdir", "readline", "readlink", "readpipe", "recv", "redo", "ref", "rename", "require", "reset", "return",
	"reverse", "rewinddir", "rindex", "rmdir", "S", "s", "say", "scalar", "seek", "seekdir", "select", "semctl", "semget", "semop", "send", "setgrent", "sethostent", "setnetent", "setpgrp", "setpriority", "setprotoent",
	"setpwent", "setservent", "setsockopt", "shift", "shmctl", "shmget", "shmread", "shmwrite", "shutdown", "sin", "sleep", "socket", "socketpair", "sort", "splice", "split", "sprintf", "sqrt", "srand", "stat", "state",
	"STDERR", "STDIN", "STDOUT", "study", "sub", "substr", "symlink", "syscall", "sysopen", "sysread", "sysseek", "system", "syswrite", "T", "t", "tell", "telldir", "tie", "tied", "time", "times", "tr", "truncate", "u",
	"uc", "ucfirst", "umask", "undef", "UNITCHECK", "unless", "unlink", "unpack", "unshift", "untie", "until", "use", "utime", "values", "vec", "w", "W", "wait", "waitpid", "wantarray", "warn", "while", "write", "X", "x",
	"xor", "y", "z"
);
my %keywordMatches;
my @masterFile;
my @commentArray;
my @quoteArray;
my @numbersArray;

sub processComment ($) {
	my $comment = shift;
	# Before we process a comment, let's make sure it isn't supposed to be a string
	if ($comment =~ /(".*#.*"|'.*#.*')/) {
		return processString($comment);
	}
	# Don't process # inside regex, but do check over the match for any other valuable info
	elsif ($comment =~ /\/.*#.*\//) {
		return processOrphan($comment);
	}
	else {
		# The results before and after a match might still contain strings or other data we may be interested in, so we filter these as well
		substr($comment, index($comment, $`), length($`), processOrphan($`));
		substr($comment, index($comment, $&), length($&), "<font id=\"comment\">$&</font>");
		substr($comment, index($comment, $'), length($'), processOrphan($'));
	}

	return $comment;
}

sub processString ($) {
	my $string = shift;
	substr($string, index($string, $`), length($`), processOrphan($`));	# filter anything preceeding our string match through processOrphan() and replace the substring with the return value
	substr($string, index($string, $&), length($&), "<font id=\"string\">$&</font>");
	substr($string, index($string, $'), length($'), processOrphan($'));	# filter anything following our string match through processOrphan() and replace the substring with the return value

	return $string;
}

# Process orphan is only intended to be called for input strings that have *already* been filtered through processComment() and processString()
sub processOrphan ($) {
	my $orphan = shift;
	my $line = "";
	# orphans might still contain strings, so make sure we are checking for them
	return processString($orphan) if $orphan =~ /(".*"|'.*')/;
	# otherwise go ahead and check for everything else
	foreach (split(/([^\w\$.])/, $orphan)) {
		# Scan for keywords
		if (exists $keywords{$_}) {
			s/($_)/<font id="keyword">$1<\/font>/g;
		}
		# Scan for numerals
		else {
			s/^([-+]?[0-9]*\.?[0-9]+)$/<font id="number">$1<\/font>/g;
		}
		$line .= $_;
	}
	return $line;
}

# Remove any POD documentation lines before we begin
$contents =~ s/^=pod.*=cut$//gms;

# Split on any line that contains a hash, is preceeded by whitespace, comma ; or brackets { } (and optionally whitespace
# after those chars) and keep the delimeter
foreach (split(/(?<=[\s;{}])(.*#.*)/, $contents)) {
	my $line = "";
	# If the split contains a valid comment, wrap it in the tags we need
	if (/(?<![\$])\s*#[^!].*/) {
		$line .= processComment($_);
	}
	# If the split doesn't contain a valid comment, split it again and go searching for strings
	else {
		foreach (split(/(".*?"|'.*?')/s)) {
			my $substring = "";
			if (/".*"|'.*'/s) {
				$substring = processString($_);
			}
			else { # If there is no string to match, then look for keywords and numbers
				$substring .= processOrphan($_);
			}

			$line .= $substring;
		}
	}

	push(@masterFile, $line);
}

open my $html, ">$output" or die "File creation failed: $!";

print $html "<!DOCTYPE html\n";
print $html "#PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\n";
print $html "#\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
print $html "<html>\n";
print $html "\n";
print $html "<head>\n";
print $html "<title>" . (split(/\//, $script))[-1] . "</title>\n";
print $html "<style>\n";
print $html "#comment {color:darkgreen;}\n";
print $html "#string {color:royalblue;}\n";
print $html "#number {color:darkcyan;}\n";
print $html "#keyword {color:darkred;}\n";
print $html "</style>\n";
print $html "</head>\n";
print $html "\n";
print $html "<body>\n";
print $html "\n";
print $html "<pre>\n";
foreach (@masterFile) { print $html "$_"; }
print $html "</pre>\n";
print $html "</body>\n";
print $html "</html>\n";