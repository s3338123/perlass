#!/usr/bin/env perl

use warnings;
use strict;
use Fcntl ":flock";

=pod

=head1 NAME

PartC.pl - Script that parses other perl scripts, highlighting vital information for the CPT223 community

=head1 B<USAGE>

Suppose the script is executable in the current path:

C<PartC.pl sample.pl>

=head1 B<OUTPUT>

Running this script will output the following information to STDOUT

=over 5

=item 1.

File information, including

=over 4

=item *

File name

=item *

Number of lines in the provided script.

=item *

Number of words in the provided script.

=item *

Number of characters in the provided script.

=back

=item 2.

Any perl keywords (as defined by assignment specs) that appear in the provided script.

=item 3.

Any numerals (as defined by assignment specs) that appear in the provided script.

=item 4.

Any strings (as defined by assignment specs) that appear in the provided script.

=item 5.

Any comments (you guessed it.. as defined by assignment specs) that appear in the provided script.

=back

=head3 B<--AUTHOR>

Luke Herron
S3338123

=cut

 # Handle incorrect script usage
die "\nUsage : PartC.pl perl_script\n" if $#ARGV + 1 != 1;
my $script = $ARGV[0];

open my $fh, $script or die "File Operation Failed: $!";
flock($fh, LOCK_SH);

die "Error: unable to analyse the specified file.\n" if not -s $fh && not -r $fh && $script !~ /\.pl|\.pm$/i;# TODO: Double check your logic here before submitting

# Engage slurp and get the file contents into one big string
local $/;
my $contents = <$fh>;
close $fh;

my $lineCount = () = $contents =~ m/\n/g;	# Match all non whitespace chars (grouped into lines), assign the results to a list, then return scalar value of list (which will store count of list elements)
my $wordCount = () = $contents =~ m/\S+/g;	# As above, except this time we are matching all non whitespace chars (grouped into 'words')
my $charCount = () = $contents =~ m/./gms;	# As above, except this time we are matching every single character in the line, including whitespace.

# Import the hardcoded keyword array into a hash
my %keywords;
$keywords{$_}++ for (
	"__DATA__", "__END__", "__FILE__", "__LINE__", "__PACKAGE__", "A", "abs", "accept", "alarm", "and", "ARGV", "ARGVOUT", "atan2", "AUTOLOAD", "B", "b", "BEGIN", "bind", "binmode", "bless", "break", "C", "c", "caller", 
	"chdir", "CHECK", "chmod", "chomp", "chop", "chown", "chr", "chroot", "close", "closedir", "cmp", "connect", "continue", "CORE", "cos", "crypt", "d", "dbmclose", "dbmopen", "defined", "delete", "DESTROY", "die", "do",
	"dump", "e", "each", "else", "elsif", "END", "endgrent", "endhostent", "endnetent", "endprotoent", "endpwent", "endservent", "eof", "eq", "eval", "exec", "exists", "exit", "exp", "f", "fcntl", "fileno", "flock", "for",
	"foreach", "fork", "format", "formline", "g", "ge", "getc", "getgrent", "getgrgid", "getgrnam", "gethostbyaddr", "gethostbyname", "gethostent", "getlogin", "getnetbyaddr", "getnetbyname", "getnetent", "getpeername",
	"getpgrp", "getppid", "getpriority", "getprotobyname", "getprotobynumber", "getprotoent", "getpwent", "getpwnam", "getpwuid", "getservbyname", "getservbyport", "getservent", "getsockname", "getsockopt", "glob",
	"gmtime", "goto", "grep", "gt", "hex", "if", "index", "INIT", "int", "ioctl", "join", "k", "keys", "kill", "l", "last", "lc", "lcfirst", "le", "length", "link", "listen", "local", "localtime", "lock", "log", "lstat",
	"lt", "M", "m", "map", "mkdir", "msgctl", "msgget", "msgrcv", "msgsnd", "my", "ne", "next", "no", "not", "O", "o", "oct", "open", "opendir", "or", "ord", "our", "p", "pack", "package", "pipe", "pop", "pos", "print",
	"printf", "prototype", "push", "q", "qq", "qr", "quotemeta", "qw", "qx", "r", "R", "rand", "read", "readdir", "readline", "readlink", "readpipe", "recv", "redo", "ref", "rename", "require", "reset", "return",
	"reverse", "rewinddir", "rindex", "rmdir", "S", "s", "say", "scalar", "seek", "seekdir", "select", "semctl", "semget", "semop", "send", "setgrent", "sethostent", "setnetent", "setpgrp", "setpriority", "setprotoent",
	"setpwent", "setservent", "setsockopt", "shift", "shmctl", "shmget", "shmread", "shmwrite", "shutdown", "sin", "sleep", "socket", "socketpair", "sort", "splice", "split", "sprintf", "sqrt", "srand", "stat", "state",
	"STDERR", "STDIN", "STDOUT", "study", "sub", "substr", "symlink", "syscall", "sysopen", "sysread", "sysseek", "system", "syswrite", "T", "t", "tell", "telldir", "tie", "tied", "time", "times", "tr", "truncate", "u",
	"uc", "ucfirst", "umask", "undef", "UNITCHECK", "unless", "unlink", "unpack", "unshift", "untie", "until", "use", "utime", "values", "vec", "w", "W", "wait", "waitpid", "wantarray", "warn", "while", "write", "X", "x",
	"xor", "y", "z"
);
my %keywordMatches;
my @commentArray;
my @quoteArray;
my @numbersArray;
my $i = 0;

sub processComment ($) {
	my $comment = shift;
	# Before we process a comment, let's make sure it isn't supposed to be a string
	if ($comment =~ /(".*#.*"|'.*#.*')/) {
		processString($comment);
	}
	# Don't process # inside regex, but do check over the match for any other valuable info
	elsif ($comment =~ /\/.*#.*\//) {
		processOrphan($comment);
	}
	else {
		# The results before and after a match might still contain strings or other data we may be interested in, so we filter these as well
		processOrphan($`);
		processOrphan($');
		push(@commentArray, $comment);
	}
}

sub processString ($) {
	my $string = shift;
	processOrphan($`);
	processOrphan($');
	push(@quoteArray, $string);
}

# This sub is only intended to be called for input strings that have *already* been filtered through processComment() and processString()
sub processOrphan ($) {
	my $orphan = shift;
	# orphans might still contain strings, so make sure we are checking for them
	if ($orphan =~ /(".*"|'.*')/) { processString($orphan); return; }
	# otherwise go ahead and check for everything else
	foreach (split(/([^\w\$.])/, $orphan)) {
		# Scan for keywords
		if (exists $keywords{$_} and not exists $keywordMatches{$_}) {
			$keywordMatches{$_} = $i;
			$i++;
		}
		# Scan for numerals
		else {
			push(@numbersArray, $1) if /^([-+]?[0-9]*\.?[0-9]+)$/;
		}
	}
}

# Remove any POD documentation lines before we begin
$contents =~ s/^=pod.*=cut$//gms;

# We assume comment lines are preceded with a new line \n, comma ; and brackets { } with optional trailing whitespace.
# Even if these matches aren't spot on, our subs above should help refine our results
foreach (split(/(?<=[\n;{}])\s*(#[^!].*)/, $contents)) {
	# We have kept the delimiter from our split, so use the same regex to see if we have the delimiter, otherwise we handle the split
	if (/(?<![\$])\s*#[^!].*/) {
		processComment($_);
	}
	else {
		# With comments out of the way, we start looking for strings
		foreach (split(/(".*?"|'.*?')/s)) {
			if (/".*"|'.*'/s) {
				processString($_);
			}
			else { # If there is no string to match, then look for keywords and numbers
				processOrphan($_);
			}
		}
	}
}

# The values of %keywordMatches contains line/char indexes of each keyword, so sorting by value means the keywords are sorted in order of appearance
my @matches = sort ({$keywordMatches{$a} <=> $keywordMatches{$b}} keys %keywordMatches);

# Set up the field and list delimiters
local $, = "\n";
local $" = "\n";

# Begin the wall o' print text
print "File: " . (split(/\//, $script))[-1] . "\n";
print "Lines: $lineCount\n";
print "Words: $wordCount\n";
print "Chars: $charCount\n";
print "[Keywords]\n";
print $#matches >= 15 ? "@matches[0..14]" . "\n" : @matches ? "@matches" . "\n" : "";
print "[Numbers]\n";
print $#numbersArray >= 10 ? "@numbersArray[0..9]" . "\n" : @numbersArray ? "@numbersArray" . "\n" : "";
print "[Strings]\n";
print $#quoteArray >= 10 ? "@quoteArray[0..9]" . "\n" : @quoteArray ? "@quoteArray" . "\n" : "";
print "[Comments]\n";
print $#commentArray >= 5 ? "@commentArray[0..4]" . "\n" : @commentArray ? "@commentArray" . "\n" : "";